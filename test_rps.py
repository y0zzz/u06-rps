import unittest
from unittest.mock import patch
from io import StringIO

class RPSGameTestCase(unittest.TestCase):
    
    @patch('builtins.input', side_effect=["rock", "scissors", "q"])
    def test_gameplay(self, mock_input):
        # Importera modulen eller funktionen som innehåller spellogiken
        from rps import play_game
        
        # Skapa en StringIO för att fånga upp utskrifterna
        output = StringIO()
        with patch('sys.stdout', new=output):
            play_game()
        
        # Hämta de fångade utskrifterna
        output_str = output.getvalue()
        
        # Kontrollera att utskrifterna innehåller förväntade strängar
        self.assertIn("Computer picked", output_str)
        self.assertIn("You won!", output_str)
        self.assertIn("You won", output_str)
        self.assertIn("The computer won", output_str)
        self.assertIn("Goodbye!", output_str)
        
        # Kontrollera att variablerna user_wins och computer_wins har rätt värden
        self.assertEqual(user_wins, 1)
        self.assertEqual(computer_wins, 0)

# Kör testerna
if __name__ == '__main__':
    unittest.main()

# Rock, Paper, Scissors

A Python implementation of the popular game Rock, Paper, Scissors.

# Description

This project is a Python implementation of the classic game Rock, Paper, Scissors. The game is played against the computer, where the player chooses to show rock, paper, or scissors. Depending on the choices made by the players, the outcome of the round is determined, and a winner is declared.
The project is developed using Python and can be executed by running the file "rps.py" in a Python environment, such as VS Code.

# Features

User-friendly interface allowing the player to choose their option by entering the corresponding number.
Proper handling of different combinations of the player's and computer's choices to determine the winner.
Presentation of the result for each round and an overall winner.

# Installation

To run the game, make sure you have Python installed on your computer. If you don't have Python installed, you can download it from the Python website and follow the installation instructions for your platform.

# Usage

Clone or download this project to your computer.

Open VS Code or any other Python-compatible editor.

Navigate to the root folder of the project.

Open the "rps.py" file.

Run the file by selecting "Run" or by running the command "python rps.py" in the terminal.

Follow the instructions in the terminal to play the game. Enter your choice by typing the number corresponding to your choice (e.g., 1 for rock, 2 for scissors, 3 for paper).
